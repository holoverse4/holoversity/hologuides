# Guides (holoVersity)


All guides are in [holoMedia][1] under [holoGuides][2]
for more info see the [guideIndex][3]


[1]: https://gitlab.com/holomedia
[2]: https://gitlab.com/holomedia/hologuides
[3]: https://gitlab.com/holomedia/hologuides/-/blob/master/guidesIndex.md
